package fr.eni.projet2.dal;

import fr.eni.projet2.bo.User;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.Optional;

@Repository
public class UserDaoImpl implements  UserDao{

    private String SELECT_BY_USERNAME = "select * from users where username = ? ";
    private String INSERT = "INSERT INTO users (email,username,password) VALUES (?, ?, ?)";

    private JdbcTemplate jdbcTemplate;

    public UserDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public  Optional<User> selectByUsername(String username) {
        User user = jdbcTemplate.queryForObject(SELECT_BY_USERNAME, new BeanPropertyRowMapper<>(User.class),username);
        Optional<User> optUser = Optional.of(user);
        return optUser;
    }

    @Override
    public void insertUser(User user) {
        this.jdbcTemplate.update(
                INSERT,
                user.getEmail(),
                user.getUsername(),
                user.getPassword()
        );
    }
}
