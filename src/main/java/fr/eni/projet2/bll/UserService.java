package fr.eni.projet2.bll;

import fr.eni.projet2.bo.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    void createUser(User user);
}
