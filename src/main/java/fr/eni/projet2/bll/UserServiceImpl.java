package fr.eni.projet2.bll;

import fr.eni.projet2.bo.User;
import fr.eni.projet2.dal.UserDao;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private UserDao userDao;

    private PasswordEncoder passwordEncoder;


    public UserServiceImpl(UserDao userDao,
                           PasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void createUser(User user) {
        // Hasher le mot passe
        user.setPassword(
                this.passwordEncoder.encode(user.getPassword())
        );
        userDao.insertUser(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("Load User");
        System.out.println(username);
        Optional<User> optUser = this.userDao.selectByUsername(username);
        if(optUser.isEmpty()){
            throw new UsernameNotFoundException(username);
        }
        return optUser.get();
    }
}
