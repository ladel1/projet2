package fr.eni.projet2;

import fr.eni.projet2.bll.UserService;
import fr.eni.projet2.bo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Projet2ApplicationTests {

    @Autowired
    private UserService userService;

    @Test
    void contextLoads() {
        User user = new User("ladel","alatibi@campus-eni.fr","123456");
        userService.createUser(user);
    }

}
