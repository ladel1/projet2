package fr.eni.projet2.dal;

import fr.eni.projet2.bo.User;

import java.util.Optional;

public interface UserDao {
    Optional<User> selectByUsername(String username);
    void insertUser(User user);
}
